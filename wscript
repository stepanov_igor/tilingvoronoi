#! /usr/bin/env python
import os
import time

project = 'TilingVoronoi'
top = '.'
out = 'build'

f_sh = 0

def options(ctx):
    ctx.load('compiler_c')
    ctx.load('compiler_cxx')

def configure(ctx):
    ctx.env.VERSION = os.popen('git rev-parse --short HEAD').read().rstrip()
    ctx.env.TIMESTAMP = str(int(time.time()))
    ctx.env.DEFINES = ['VERSION=0x'+ctx.env.VERSION, 'COMPILE_TIMESTAMP='+ctx.env.TIMESTAMP, 'DEBUG=0', 'WAF=1']
    print('→ configuring the project')
    ctx.find_program('strip', var='STRIP')
    ctx.find_program('gcc', var='CC')
    ctx.find_program('g++', var='CXX')
    ctx.load('compiler_c')
    ctx.load('compiler_cxx qt5')
    ctx.env.CFLAGS = '-Os -fPIC'.split()
    ctx.env.CFLAGS += '-g -pg -ggdb -Wno-unused-variable -Wno-format -Wno-unused-but-set-variable -Wall -fopenmp'.split()
    ctx.env.CXXFLAGS = [] + ctx.env.CFLAGS
    ctx.env.CFLAGS += '-std=gnu99'.split()
    ctx.env.CXXFLAGS += '-std=gnu++1z'.split()
    ctx.env.LDFLAGS = '-pg  -fopenmp'.split()
    #ctx.load('qt5')

def clean(ctx):
    print('Cleaned.')

def build(ctx):
#    ctx.add_post_fun(post)
    libs = 'libusb-1.0'.split()
    lib_flags_c = []
    lib_flags_ld = []
    print(ctx.env)
    for lib in libs:
        lib_flags_c += os.popen('pkg-config --cflags ' + lib).read().rstrip().split()
        lib_flags_ld += os.popen('pkg-config --libs ' + lib + ' | sed -e "s/-l//" ').read().rstrip().split()
    source_objlib=['src/' + s for s in 'Voronoi.cc'.split()]
    source_TilingVoronoi=['src/' + s for s in 'TilingVoronoi.cc PolyWindow.cc'.split()]
    moc_tiling=['src/' + s for s in 'PolyWindow.hh'.split()]
    ctx.objects(target='objlib', source = source_objlib, cxxflags = lib_flags_c, includes = ctx.env.INCLUDES_QT5WIDGETS + 'build/src /usr/local/include'.split())
    ctx(target='TilingVoronoi', source = source_TilingVoronoi, includes = ctx.env.INCLUDES_QT5WIDGETS + 'build/src /usr/local/include'.split(), moc = moc_tiling, uselib = ctx.env.LIB_QT5WIDGETS, use=['objlib'], lib='pthread mpfr gmp'.split() + ctx.env.LIB_QT5WIDGETS + ctx.env.LIB_QT5SVG + lib_flags_ld, features='qt5 cxx cxxprogram')

from waflib import TaskGen

