#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDesktopWidget>
#include <QtGui/QWindow>
#include <QtCore/QCommandLineParser>
#include <QtGui/QPdfWriter>
#include <QtSvg/QSvgGenerator>
#include <unistd.h>
#include "PolyWindow.hh"
#include <glm/vec2.hpp>
#include <math.h>
#include "Voronoi.hh"

PentaType3::PentaType3(float alpha)
{
    vertexList = {   // points of base cell
                     {0.0, 0.0}, //O
                     {0.0, 1.0}, {-sqrt(3)/2.0, 1.0/2.0}, {-sqrt(3)/2.0, -1.0/2.0},    //A B C
                     {0.0, -1.0}, {sqrt(3)/2.0, -1.0/2.0}, {sqrt(3)/2.0, 1.0/2.0},   //D E F
                     {0, 0}, {0, 0}, {0, 0}     // R(7) S(8) T(9)
                 };
    tileList = {   // point indices to form tiles of base cell
                   {0, 7, 1, 2, 9, 0}, {0, 9, 3, 4, 8, 0}, {0, 8, 5, 6, 7, 0}
               };
    translateX = {sqrt(3), 0.0};
    translateY = {sqrt(3)/2.0, 3.0/2.0};
    vertexList[7] = 0.5f * ( vertexList[1] * (1.0f - alpha) + vertexList[6] * (1.0f + alpha));
    vertexList[8] = 0.5f * ( vertexList[5] * (1.0f - alpha) + vertexList[4] * (1.0f + alpha));
    vertexList[9] = 0.5f * ( vertexList[3] * (1.0f - alpha) + vertexList[2] * (1.0f + alpha));
}

static const TileCell pentaType4 =
{
    {   // points of base cell
        {0.0, 0.0},
        {0, 1}, {1, 1},  {1.5, 0.5},  {1, 0},
        {1, -1}, {0.5, -1.5},  {0, -1},  {-1, -1},
        {-1.5, -0.5}, {-1, 0},  {-1, 1},  {-0.5, 1.5}
    },
    {   // point indices to form tiles of base cell
        {0, 1, 2, 3, 4, 0}, {0, 4, 5, 6, 7, 0}, {0, 7, 8, 9, 10, 0}, {0, 10, 11, 12, 1, 0}
        //        {0, 7, 8, 9, 10, 0}, {0, 10, 11, 12, 1, 0}
    },  // X and Y translation vectors ob base cell
    {2.0, -1.0},
    {1.0, 2.0}
};

extern QRect g_screenGeometry;

PolyWindow::PolyWindow(QRect geom, int xCount, int yCount, QWidget *parent)
    : geom(geom), xCount(xCount), yCount(yCount), QWidget(parent)
{
    image = new QImage(geom.width(), geom.height(), QImage::Format_ARGB32);
    image->fill(Qt::black);
    connect(this, &PolyWindow::SignalDrawFinger, this, &PolyWindow::SlotDrawFinger);
    connect(this, &PolyWindow::SignalDrawLattice, this, &PolyWindow::SlotDrawLattice);
    connect(this, &PolyWindow::SignalDrawAim, this, &PolyWindow::SlotDrawAim);
}

PolyWindow::~PolyWindow() {
}

void PolyWindow::paintEvent(QPaintEvent *event) {
    QPainter paint(this);
    paint.drawImage(QPoint(0,0), *image, this->rect());
}

void PolyWindow::mousePressEvent(QMouseEvent *event) {
}

void PolyWindow::mouseMoveEvent(QMouseEvent *event) {
    int x = event->pos().x();
    int y = event->pos().y();
    SlotDrawFinger(event->pos());
}

void PolyWindow::SlotDrawFinger(QPoint point) {
    static int h = 0;
    QPainter paint(image);
    QPainterPath circlePath;
    int x = point.x();
    int y = point.y();
    paint.setPen(QColor(0, x & 0xff, y & 0xff, 192));
    circlePath.addEllipse(x - 5, y - 5, 10, 10);
    //    paint.fillPath(circlePath, QBrush(QColor(0, event->pos().x() & 0xff, event->pos().y() & 0xff, 192)));
    paint.fillPath(circlePath, QBrush(QColor::fromHsv(h++ % 360, 192, 192, 64)));
    this->update(QRect(x - 10, y - 10, 20, 20));
}

void PolyWindow::DrawCircle(QPainter &paint, QPoint const &point, QBrush const &brush) {
    QPainterPath circlePath;
    int x = point.x();
    int y = point.y();
    circlePath.addEllipse(x - 5, y - 5, 10, 10);
    paint.fillPath(circlePath, brush);
}

void PolyWindow::DrawText(QPainter &paint, QPoint const &point, QString const &text, QPen const &pen) {
    int x = point.x();
    int y = point.y();
    QRect rect = QRect(point, QSize(100, 20));
    paint.setPen(pen);
    paint.drawText(rect, text);
}

void PolyWindow::SlotDrawLattice() {
    QPainter paint(image);
    QPainterPath circlePath;
    QRect geom = this->geometry();
    glm::vec2 edge_left, edge_right, delta_edge, delta_lattice;
    delta_edge.x = (float)geom.width() / 32.0;
    delta_edge.y = (float)geom.height() / 32.0;
    edge_left.x = delta_edge.x;
    edge_right.x = (float)geom.width() - delta_edge.x;
    edge_left.y = delta_edge.y;
    edge_right.y = (float)geom.height() - delta_edge.y;
    delta_lattice.x = (edge_right.x - edge_left.x) / (xCount - 1);
    delta_lattice.y = (edge_right.y - edge_left.y) / (yCount - 1);
    for(int j = 0; j < yCount; j ++) {
        for(int i = 0; i < xCount; i ++) {
            int x = edge_left.x + i * delta_lattice.x;
            int y = edge_left.y + j * delta_lattice.y;
            paint.setPen(QColor::fromHsv(359, 0, 128, 255));
            circlePath.addEllipse(x - 8, y - 8, 16, 16);
            paint.drawPath(circlePath);
        }
    }
    for(int i = 0; i < xCount; i ++) {
        int x = edge_left.x + i * delta_lattice.x;
        paint.setPen(QColor::fromHsv(359, 0, 128, 255));
        paint.drawLine(x, 0, x, geom.height());
    }
    for(int i = 0; i < yCount; i ++) {
        int y = edge_left.y + i * delta_lattice.y;
        paint.setPen(QColor::fromHsv(359, 0, 128, 255));
        paint.drawLine(0, y, geom.width(), y);
    }
    this->update(this->geometry());
}

void PolyWindow::SlotDrawAim(int ix, int iy, int v) {
    QPainter paint(image);
    QPainterPath circlePath;
    QRect geom = this->geometry();
    glm::vec2 edge_left, edge_right, delta_edge, delta_lattice;
    delta_edge.x = (float)geom.width() / 32.0;
    delta_edge.y = (float)geom.height() / 32.0;
    edge_left.x = delta_edge.x;
    edge_right.x = (float)geom.width() - delta_edge.x;
    edge_left.y = delta_edge.y;
    edge_right.y = (float)geom.height() - delta_edge.y;
    delta_lattice.x = (edge_right.x - edge_left.x) / (xCount - 1);
    delta_lattice.y = (edge_right.y - edge_left.y) / (yCount - 1);
    int x = edge_left.x + ix * delta_lattice.x;
    int y = edge_left.y + iy * delta_lattice.y;
    if(v) paint.setPen(QColor::fromHsv(359, 0, 255, 255));
    else paint.setPen(QColor::fromHsv(359, 0, 128, 255));
    circlePath.addEllipse(x - 8, y - 8, 16, 16);
    paint.drawPath(circlePath);
    this->update(QRect(x - 10, y - 10, 20, 20));
}

void PolyWindow::SlotDrawPenta() {
    QPainter paint(image);
    Voronoi voronoi;
    const TileCell &cell = PentaType3(0.5);
    std::list<glm::vec2> pointList;
    std::list<std::vector<glm::vec2>> edgeList;
    std::list<glm::vec2> uniqPointList;
    std::list<glm::vec2> voronoiVertexList;
    std::list<std::vector<glm::vec2>> voronoiFaceList;
    std::list<std::vector<glm::vec2>> voronoiEdgeList;
    paint.setPen(QColor::fromHsv(359, 0, 128, 255));
    QFont font(paint.font());
    font.setBold(0);
    font.setWeight(2);
    paint.setFont(font);
    for(int j = 0; j < 20; j ++) {
        for(int i = 0; i < 20; i ++) {
            for(const auto &poly : cell.tileList) {
                int vertexCnt = poly.size();
                for(int k = 0; k < (vertexCnt - 1); k++) {
                    glm::vec2 v0 = cell.vertexList[poly[k]];
                    glm::vec2 v1 = cell.vertexList[poly[k + 1]];
                    glm::vec2 translate = (float)(i - 10) * cell.translateX + (float)(j - 10) * cell.translateY;
                    v0 += translate;
                    v1 += translate;
                    v0 *= 100.0;
                    v1 *= 100.0;
                    //                    paint.setPen(QColor::fromHsv(((i + j * 20) * 5 + k)%360, 255, 255, 128));
                    //                    paint.setPen(QColor::fromHsv(((i + j * 20) * 5 + k)%360, 0, 0, 255));
                    //                    paint.drawLine(v0.x, v0.y, v1.x, v1.y);
                    pointList.push_back(v0);
                    edgeList.push_back({{v0.x, v0.y}, {v1.x, v1.y}});
                    //                    DrawText(paint, QPoint(v0.x, v0.y), QString("[%1,%2,%3]").arg(i).arg(j).arg(k), paint.pen());
                    //                    DrawCircle(paint, QPoint(v0.x, v0.y), QBrush(QColor::fromHsv(((i + j * 20) * 5 + k)%360, 255, 128)));
                    //                    DrawCircle(paint, QPoint(v0.x, v0.y), QBrush(QColor::fromHsv(((i + j * 20) * 5 + k)%360, 0, 0)));
                    printf("%d -- %d\n", poly[k], poly[k+1]);
                }
                printf("\n");
            }
        }
    }
    //remove coincident points from list (create list with uniq points)
    voronoi.GetUniquePoints(pointList, uniqPointList);
    // display vertices from uniq points
    int hue = 0;
    for(auto v: uniqPointList) {
        DrawText(paint, QPoint(v.x, v.y), QString("[%1]").arg(hue), QPen(QColor::fromHsv(hue % 360, 255, 128)));
        DrawCircle(paint, QPoint(v.x, v.y), QBrush(QColor::fromHsv(hue % 360, 255, 128)));
        hue ++;
    }
    // display edges
    for(auto v: edgeList) {
        paint.setPen(QColor::fromHsv(hue%360, 255, 255, 128));
        paint.setPen(QColor::fromHsv(hue%360, 0, 0, 255));
        paint.drawLine(v[0].x, v[0].y, v[1].x, v[1].y);
        hue ++;
    }
    // now compute Voronoi
    voronoi.Compute(uniqPointList, voronoiVertexList, voronoiEdgeList, voronoiFaceList);
    //    voronoiFaceList.push_back({{0, 0}, {100, 100}, {100, 0}}); //test
    for(auto face: voronoiFaceList) {
        QPolygon polygon;
        for(auto v: face) {
            polygon << QPoint(v.x, v.y);
        }
        paint.setPen(QPen(QColor::fromHsv(hue % 360, 255, 255, 128)));
        paint.setPen(QPen());
        paint.setBrush(QBrush(QColor::fromHsv(hue % 360, 255, 255, 64)));
        hue += 93;
        paint.drawPolygon(polygon);
    }
    for(auto edge: voronoiEdgeList) {
        QPolygon polygon;
        for(auto v: edge) {
            polygon << QPoint(v.x, v.y);
        }
        paint.setPen(QPen(QColor::fromHsv(hue++ % 360, 192, 192, 192)));
        paint.setBrush(QBrush());
        //        paint.drawPolygon(polygon);
    }
    this->update(this->geometry());
}

void PolyWindow::SvgDrawPenta() {
    auto svg = new QSvgGenerator;
    svg->setFileName("tile.frame.svg");
    QPainter paint;
    paint.begin(svg);
    Voronoi voronoi;
    const TileCell &cell = PentaType3(1.);
    std::list<glm::vec2> pointList;
    std::list<std::vector<glm::vec2>> edgeList;
    std::list<glm::vec2> uniqPointList;
    std::list<glm::vec2> voronoiVertexList;
    std::list<std::vector<glm::vec2>> voronoiFaceList;
    std::list<std::vector<glm::vec2>> voronoiEdgeList;
    paint.setPen(QColor::fromHsv(359, 0, 128, 255));
    QFont font(paint.font());
    font.setBold(0);
    font.setWeight(2);
    paint.setFont(font);
    for(int j = 0; j < 20; j ++) {
        for(int i = 0; i < 20; i ++) {
            for(const auto &poly : cell.tileList) {
                int vertexCnt = poly.size();
                for(int k = 0; k < (vertexCnt - 1); k++) {
                    glm::vec2 v0 = cell.vertexList[poly[k]];
                    glm::vec2 v1 = cell.vertexList[poly[k + 1]];
                    glm::vec2 translate = (float)(i - 10) * cell.translateX + (float)(j - 10) * cell.translateY;
                    v0 += translate;
                    v1 += translate;
                    v0 *= 100.0;
                    v1 *= 100.0;
                    //                    paint.setPen(QColor::fromHsv(((i + j * 20) * 5 + k)%360, 255, 255, 128));
                    //                    paint.setPen(QColor::fromHsv(((i + j * 20) * 5 + k)%360, 0, 0, 255));
                    //                    paint.drawLine(v0.x, v0.y, v1.x, v1.y);
                    pointList.push_back(v0);
                    edgeList.push_back({{v0.x, v0.y}, {v1.x, v1.y}});
                    //                    DrawText(paint, QPoint(v0.x, v0.y), QString("[%1,%2,%3]").arg(i).arg(j).arg(k), paint.pen());
                    //                    DrawCircle(paint, QPoint(v0.x, v0.y), QBrush(QColor::fromHsv(((i + j * 20) * 5 + k)%360, 255, 128)));
                    //                    DrawCircle(paint, QPoint(v0.x, v0.y), QBrush(QColor::fromHsv(((i + j * 20) * 5 + k)%360, 0, 0)));
                    printf("%d -- %d\n", poly[k], poly[k+1]);
                }
                printf("\n");
            }
        }
    }
    //remove coincident points from list (create list with uniq points)
    voronoi.GetUniquePoints(pointList, uniqPointList);
    // display vertices from uniq points
    int hue = 0;
    for(auto v: uniqPointList) {
        DrawText(paint, QPoint(v.x, v.y), QString("[%1]").arg(hue), QPen(QColor::fromHsv(hue % 360, 255, 128)));
        DrawCircle(paint, QPoint(v.x, v.y), QBrush(QColor::fromHsv(hue % 360, 255, 128)));
        hue ++;
    }
    // display edges
    for(auto v: edgeList) {
        paint.setPen(QColor::fromHsv(hue%360, 255, 255, 128));
        paint.setPen(QColor::fromHsv(hue%360, 0, 0, 255));
        paint.drawLine(v[0].x, v[0].y, v[1].x, v[1].y);
        hue ++;
    }
    paint.end();
    delete svg;
    svg = new QSvgGenerator;
    svg->setFileName("voronoi.frame.svg");
    paint.begin(svg);
    // now compute Voronoi
    voronoi.Compute(uniqPointList, voronoiVertexList, voronoiEdgeList, voronoiFaceList);
    //    voronoiFaceList.push_back({{0, 0}, {100, 100}, {100, 0}}); //test
    for(auto face: voronoiFaceList) {
        QPolygon polygon;
        for(auto v: face) {
            polygon << QPoint(v.x, v.y);
        }
        paint.setPen(QPen(QColor::fromHsv(hue % 360, 255, 255, 128)));
        paint.setPen(QPen());
        paint.setBrush(QBrush(QColor::fromHsv(hue % 360, 255, 255, 64)));
        hue += 93;
        paint.drawPolygon(polygon);
    }
    for(auto edge: voronoiEdgeList) {
        QPolygon polygon;
        for(auto v: edge) {
            polygon << QPoint(v.x, v.y);
        }
        paint.setPen(QPen(QColor::fromHsv(hue++ % 360, 192, 192, 192)));
        paint.setBrush(QBrush());
        //        paint.drawPolygon(polygon);
    }
    for(auto v: uniqPointList) {
        DrawText(paint, QPoint(v.x, v.y), QString("[%1]").arg(hue), QPen(QColor::fromHsv(hue % 360, 255, 128)));
        DrawCircle(paint, QPoint(v.x, v.y), QBrush(QColor::fromHsv(hue % 360, 255, 128)));
        hue ++;
    }
    paint.end();
    delete svg;
    svg = new QSvgGenerator;
    svg->setFileName("voronoi.reverse.svg");
    paint.begin(svg);
    pointList.clear();
    //remove coincident points from list (create list with uniq points)
    voronoi.GetUniquePoints(voronoiVertexList, pointList);
    voronoiEdgeList.clear();
    voronoiFaceList.clear();
    voronoiVertexList.clear();
    voronoi.Compute(pointList, voronoiVertexList, voronoiEdgeList, voronoiFaceList);

//    pointList.clear();
//    voronoi.GetUniquePoints(voronoiVertexList, pointList);
//    voronoi.Compute(pointList, voronoiVertexList, voronoiEdgeList, voronoiFaceList);
//    voronoiEdgeList.clear();
//    voronoiFaceList.clear();
//    voronoiVertexList.clear();
//    voronoi.Compute(pointList, voronoiVertexList, voronoiEdgeList, voronoiFaceList);

    //    pointList.clear();
    //    voronoi.GetUniquePoints(voronoiVertexList, pointList);
    //    voronoi.Compute(pointList, voronoiVertexList, voronoiEdgeList, voronoiFaceList);
    //    voronoiEdgeList.clear();
    //    voronoiFaceList.clear();
    //    voronoiVertexList.clear();
    //    voronoi.Compute(pointList, voronoiVertexList, voronoiEdgeList, voronoiFaceList);

    //    voronoiFaceList.push_back({{0, 0}, {100, 100}, {100, 0}}); //test
    for(auto face: voronoiFaceList) {
        QPolygon polygon;
        for(auto v: face) {
            polygon << QPoint(v.x, v.y);
        }
        paint.setPen(QPen(QColor::fromHsv(hue % 360, 255, 255, 192)));
        paint.setPen(QPen());
        paint.setBrush(QBrush(QColor::fromHsv(hue % 360, 255, 255, 128)));
        hue += 93;
        paint.drawPolygon(polygon);
    }
    for(auto edge: voronoiEdgeList) {
        QPolygon polygon;
        for(auto v: edge) {
            polygon << QPoint(v.x, v.y);
        }
        paint.setPen(QPen(QColor::fromHsv(hue++ % 360, 192, 192, 192)));
        paint.setBrush(QBrush());
        //        paint.drawPolygon(polygon);
    }
    for(auto v: uniqPointList) {
        DrawText(paint, QPoint(v.x, v.y), QString("[%1]").arg(hue), QPen(QColor::fromHsv(hue % 360, 255, 128)));
        DrawCircle(paint, QPoint(v.x, v.y), QBrush(QColor::fromHsv(hue % 360, 255, 128)));
        hue ++;
    }
    paint.end();
    delete svg;
    this->update(this->geometry());
}

void PolyWindow::DrawFinger(QPoint point) {
    emit SignalDrawFinger(point);
}

void PolyWindow::DrawAim(int ix, int iy, int v) {
    emit SignalDrawAim(ix, iy, v);
}

void PolyWindow::DrawLattice() {
    //    emit SignalDrawLattice();
    emit SlotDrawPenta();
}

extern PolyWindow *g_calibWindow;

