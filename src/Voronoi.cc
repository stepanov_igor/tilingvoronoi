#include "Voronoi.hh"

#include <iterator>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_triangulation_adaptation_traits_2.h>
#include <CGAL/Delaunay_triangulation_adaptation_policies_2.h>
#include <CGAL/Regular_triangulation_adaptation_traits_2.h>
#include <CGAL/Regular_triangulation_adaptation_policies_2.h>
#include <CGAL/Regular_triangulation_2.h>
#include <CGAL/Voronoi_diagram_2.h>
#include <CGAL/Boolean_set_operations_2.h>
#include <CGAL/bounding_box.h>
#include <CGAL/Polygon_2.h>

#include <glm/vec2.hpp>
#include <glm/glm.hpp>


// typedefs for defining the adaptor
typedef CGAL::Exact_predicates_inexact_constructions_kernel                  K;
typedef CGAL::Delaunay_triangulation_2<K>                                    DT;
typedef CGAL::Delaunay_triangulation_adaptation_traits_2<DT>                 AT;
typedef CGAL::Delaunay_triangulation_caching_degeneracy_removal_policy_2<DT> AP;
typedef CGAL::Voronoi_diagram_2<DT,AT,AP>                                    VD;
// typedef for the result type of the point location
typedef AT::Site_2                    Site_2;
typedef AT::Point_2                   Point_2;
typedef VD::Locate_result             Locate_result;
typedef VD::Vertex_handle             Vertex_handle;
typedef VD::Face_handle               Face_handle;
typedef VD::Halfedge_handle           Halfedge_handle;
typedef VD::Ccb_halfedge_circulator   Ccb_halfedge_circulator;


int Voronoi::Compute(std::list<glm::vec2> const &inputPoints, std::list<glm::vec2> &voronoiVertexList, std::list<std::vector<glm::vec2>> &voronoiEdgeList, std::list<std::vector<glm::vec2>> &voronoiFaceList) {
    VD vd;
    Site_2 t;
    for(auto v: inputPoints) {
        //        points.push_back(Point_2(v.x, v.y));
        vd.insert(Site_2(v.x, v.y));
    }
    for(VD::Face_iterator f = vd.faces_begin(); f != vd.faces_end(); ++f) {
        Ccb_halfedge_circulator cir = f->ccb();
        Ccb_halfedge_circulator cir_start = cir;
        std::vector<glm::vec2> vertices;
        int i = 0;
        do {
            glm::vec2 p1, p2;
            if(cir->has_source()) {
                p1.x = cir->source()->point().x();
                p1.y = cir->source()->point().y();
            } else i = 1;
            if(cir->has_target()) {
                p2.x = cir->target()->point().x();
                p2.y = cir->target()->point().y();
            } else i = 1;
            if(glm::distance(p1, p2) > 1000.0) i = 1;
            vertices.push_back(glm::vec2(p1.x, p1.y));
        } while ( ++cir != cir_start);
        if(!i){
            voronoiFaceList.push_back(vertices);
            for(auto v: vertices) {
                voronoiVertexList.push_back(v);
            }
        }
    }
    return 0;
}

//remove coincident points from list (create list with uniq points)
int Voronoi::GetUniquePoints(std::list<glm::vec2> const &inputPoints, std::list<glm::vec2> &uniqPointList) {
    for(auto p: inputPoints) {
        int uniq = 0;
        for(auto up: uniqPointList) {
            if(glm::distance(up, p) < 0.001) uniq = 1;
        }
        if(!uniq) uniqPointList.push_front(p);
    }
    return 0;
}
