#pragma once

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDesktopWidget>
#include <QtGui/QWindow>
#include <QtCore/QCommandLineParser>
#include <QtGui/QImage>
#include <QtGui/QPainter>
#include <QtGui/QMouseEvent>
#include <QtGui/QPainterPath>
#include <QtCore/QThread>
#include <QtCore/QMutex>
#include <QtCore/QSemaphore>
#include <glm/vec2.hpp>
#include <glm/mat2x2.hpp>
#include <glm/glm.hpp>

struct TileCell
{
public:
    std::vector<glm::vec2> vertexList;
    std::vector<std::vector<int>> tileList;
    glm::vec2 translateX;
    glm::vec2 translateY;
};

struct PentaType3 : public TileCell
{
    PentaType3(float alpha = 0.0);
};

class PolyWindow : public QWidget
{
    Q_OBJECT
public:
    QImage *image;
    QRect geom;
    int xCount, yCount;
    PolyWindow(QRect geom, int xCount, int yCount, QWidget *parent = 0);
    ~PolyWindow();
    virtual void paintEvent(QPaintEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    void DrawFinger(QPoint point);
    void DrawLattice();
    void DrawAim(int ix, int iy, int v);
public slots:
    void SlotDrawFinger(QPoint point);
    void DrawCircle(QPainter &paint, QPoint const &point, QBrush const &brush);
    void DrawText(QPainter &paint, QPoint const &point, QString const &text, QPen const &pen);
    void SvgDrawPenta();
    void SlotDrawLattice();
    void SlotDrawPenta();
    void SlotDrawAim(int ix, int iy, int v);
signals:
    void SignalDrawFinger(QPoint point);
    void SignalDrawLattice();
    void SignalDrawAim(int ix, int iy, int v);
};
