#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>
#include <QtWidgets/QDesktopWidget>
#include <QtGui/QWindow>
#include <QtCore/QCommandLineParser>
#include "PolyWindow.hh"
#include <unistd.h>

PolyWindow *g_calibWindow;
QRect g_screenGeometry;

int main(int argc, char *argv[])
{
    QCommandLineParser cmdline;
    QApplication a(argc, argv);
    PolyWindow *polyWindow;
    cmdline.addOptions({
                           {{"p", "port"}, "usb tree port chain, string from enumeration.", "port", ""},
                           {{"s", "screen"}, "set screen number to show window, integer.", "screen", "0"},
                           {{"x", "xcount"}, "set horizontal points, integer.", "xcnt", "16"},
                           {{"y", "ycount"}, "set vertical points, integer.", "ycnt", "10"},
                           {{"e", "enum"}, "enumerate Microtouch devices\n"},
                           {{"h", "help"}, "x:y = 16:10 e.g. [16,10] [10,6]\n" "Show this info."}
                       });
    cmdline.process(a);
    int screenindex = cmdline.value("screen").toInt();
    int xCount = cmdline.value("xcount").toInt();
    int yCount = cmdline.value("ycount").toInt();
    if(cmdline.isSet("help")){
        cmdline.showHelp();
        return 0;
    }
    a.setApplicationName("Pentagon type 3 tiling and Voronoi");
    printf("i=%i\n", screenindex);
    g_screenGeometry = a.desktop()->screenGeometry(screenindex);
    polyWindow = new PolyWindow(g_screenGeometry, xCount, yCount);
    g_calibWindow = polyWindow;
    polyWindow->show();
    polyWindow->setGeometry(g_screenGeometry);
    polyWindow->showFullScreen();
    polyWindow->DrawLattice();
    polyWindow->SvgDrawPenta();
    return a.exec();
    return 0;
}

