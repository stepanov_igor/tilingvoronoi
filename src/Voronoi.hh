#pragma once

#include <list>
#include <vector>
#include <glm/vec2.hpp>
#include <glm/glm.hpp>

class Voronoi
{
public:
    Voronoi() {}
    ~Voronoi() {}
    int Compute(std::list<glm::vec2> const &inputPoints, std::list<glm::vec2> &voronoiVertexList, std::list<std::vector<glm::vec2>> &voronoiEdgeList, std::list<std::vector<glm::vec2>> &voronoiFaceList);
    int GetUniquePoints(std::list<glm::vec2> const &inputPoints, std::list<glm::vec2> &uniqPointList);
};
